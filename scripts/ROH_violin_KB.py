import seaborn as sns
import sys
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
matplotlib.use('Agg')

df = pd.read_csv(sys.argv[1],sep='\t',header=0) ## Read ROH results in dataframe
df['KB'] = (df['KB']*1000)/float(890000000) ## Calculate Froh

fig, axes = plt.subplots()
plt.figure(figsize=(10,8))

## Plot Froh
ax = sns.boxplot(x="FID", y="KB", data=df)
ax.set(xlabel='Breed', ylabel='Froh')
plt.savefig('PigROH_Froh.pdf')

## Plot average ROH length
ax = sns.boxplot(x="FID", y="KBAVG", data=df)
ax.set(xlabel='Breed', ylabel='Average-ROH-Length-KB')
plt.savefig('PigROH_AVG_Length.pdf')

